// Soal No. 1 (Array to Object)
function arrayToObject(arr) {
    const age = (birthYear) => (new Date()).getFullYear() - birthYear >= 0 ? (new Date()).getFullYear() - birthYear : 'Invalid Birth Year'
    let obj = {}
    for (let i in arr) {
        obj[`${Number(i) + 1}. ${arr[i][0]} ${arr[i][1]}`] = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: arr[i][3] ? age(arr[i][3]) : 'Invalid Birth Year'
        }
    }

    console.log(obj)
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2050]]
arrayToObject(people2)

arrayToObject([])

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId = null, money = null) {
    if (!memberId || !money) return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    if (money < 50000) return 'Mohon maaf, uang tidak cukup'
    let items = [
        ['Sepatu Stacattu', 1500000],
        ['Baju Zoro', 500000],
        ['Baju H&N', 250000],
        ['Sweater Uniklooh', 175000],
        ['Casing Handphone', 50000]
    ]

    let obj = {
        memberId,
        listPurchased: [],
        changeMoney: money
    }

    for (let item of items) {
        if (obj.changeMoney - item[1] >= 0) {
            obj.listPurchased.push(item[0])
            obj.changeMoney -= item[1]
        }
    }

    return obj
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000)) //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)) //Mohon maaf, uang tidak cukup
console.log(shoppingTime()) //Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    var rute = {'A':1, 'B':2, 'C':3, 'D':4, 'E':5, 'F':6}
    let obj = []
    for (let arr of arrPenumpang) {
        obj.push({
            penumpang: arr[0],
            naikDari: arr[1],
            tujuan: arr[2],
            bayar: 2000 * (rute[arr[2]] - rute[arr[1]])
        })
    }

    return obj
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))