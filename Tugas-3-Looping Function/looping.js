// No. 1 Looping While
console.log('-----SOAL 1-----')
let idx = 1
while(idx < 20) {
    if (idx == 1) console.log('LOOPING PERTAMA')
    if (idx <= 10) console.log(`${20 - Math.abs(2*idx - 20)} - I love coding`)
    if (idx == 10) console.log('LOOPING KEDUA')
    if (idx >= 10) console.log(`${20 - Math.abs(2*idx - 20)} - I will become a mobile developer`)
    idx++
}
console.log('\n')

// No. 2 Looping menggunakan for
console.log('-----SOAL 2-----')
for (let i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) console.log(`${i} - I Love Coding`)
    else if (i % 2 == 1) console.log(`${i} - Santai`)
    else console.log(`${i} - Berkualitas`)
}
console.log('\n')

// No. 3 Membuat Persegi Panjang #
console.log('-----SOAL 3-----')
for (let i = 0; i < 4; i++) console.log('########')
console.log('\n')

// No. 4 Membuat Tangga
console.log('-----SOAL 4-----')
let pagar = '#'
for (let i = 0; i < 7; i++) {
    console.log(pagar)
    pagar += '#'
}
console.log('\n')

// No. 5 Membuat Papan Catur
console.log('-----SOAL 5-----')
for(let i = 0; i < 8; i++) for(let j = 0; j < 8; j++) j == 7 ? i % 2 == 0 ? process.stdout.write('#\n') : process.stdout.write('\n') : (i+j) % 2 == 0 ? process.stdout.write(' ') : process.stdout.write('#')