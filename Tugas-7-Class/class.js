// no 1
// releas0
class Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name
        this.legs = legs
        this.cold_blooded = cold_blooded
    }
}

let sheep = new Animal("shaun")

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// release1
class Frog extends Animal {
    constructor(name, legs = 4, cold_blooded = false) {
        super(name, legs, cold_blooded)
    }

    jump() {
        console.log("hop hop")
    }
}

class Ape extends Animal {
    constructor(name, legs = 2, cold_blooded = false) {
        super(name, cold_blooded)
        this.legs = legs
    }

    yell() {
        console.log("Auooo")
    }
}

let sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

let kodok = new Frog("buduk")
kodok.jump() // "hop hop"

// no 2
class Clock {
    constructor(template) {
        this.template = template.template
        this.timer = null
    }

    render() {
        let date = new Date()

        let hours = date.getHours()
        if (hours < 10) hours = "0" + hours

        let mins = date.getMinutes()
        if (mins < 10) mins = "0" + mins

        let secs = date.getSeconds()
        if (secs < 10) secs = "0" + secs
        let output = this.template
            .replace("h", hours)
            .replace("m", mins)
            .replace("s", secs)

        console.log(output)
    }

    stop() {
        clearInterval(this.timer)
    }

    start() {
        this.timer = setInterval(() => {
            this.render()
        }, 1000)
    }
}

let clock = new Clock({ template: "h:m:s" })
clock.start()
