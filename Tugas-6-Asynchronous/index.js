const readBook = require('./callback.js')

const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]


let waktu = 10000

const baca = (waktu, index) => {
    if (index == books.length) return
    readBook(waktu, books[index], (sisaWaktu) => {
        if (sisaWaktu > 0 && index < books.length) {
            index++
            baca(sisaWaktu, index)
        }
    })
}

baca(waktu, 0)