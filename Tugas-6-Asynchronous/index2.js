const readBookPromise = require('./promise.js')

const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let waktu = 1000

const baca = (waktu, index) => {
    if (index == books.length) return
    readBookPromise(waktu, books[index])
        .then(sisaWaktu => {
            index++
            baca(sisaWaktu, index)
        })
        .catch(err => { })
}

baca(waktu, 0)