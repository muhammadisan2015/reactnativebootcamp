// Soal No. 1 (Range) 
function range(startNum, finishNum) {
    let arr = []
    for (let i = startNum; i <= finishNum; i++) arr.push(i)
    return arr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    let arr = []
    for (let i = startNum; i <= finishNum; i += step) arr.push(i)
    return arr
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step = null) {
    if (!step) return -1
    let sum = 0
    for (let i = startNum; i <= finishNum; i += step) sum += i
    return sum
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// Soal No. 4 (Array Multidimensi)

function dataHandling(arr) {
    for (let ar of arr) {
        console.log(`Nomor ID: ${ar[0]}`)
        console.log(`Nama Lengkap: ${ar[1]}`)
        console.log(`TTL: ${ar[2]} ${ar[3]}`)
        console.log(`Hobi: ${ar[4]}`)
        console.log('\n')
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

// Soal No. 5 (Balik Kata)
function balikKata(str) {
    let st = ''
    for (let i = str.length-1; i >= 0; i--) st += str[i]
    return st
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal No. 6 (Metode Array)
// males bangets